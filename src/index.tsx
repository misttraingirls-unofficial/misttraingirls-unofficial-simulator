import React from "react";
import ReactDOM from "react-dom";
import "index.css";
import App from "components/App";
import reportWebVitals from "reportWebVitals";
import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles";

// reference: https://material.io/resources/color/
const theme = createMuiTheme({
  palette: {
    primary: {
      light: "#62727b",
      main: "#37474f",
      dark: "#102027",
      contrastText: "#ffffff",
    },
    secondary: {
      light: "#ffffe4",
      main: "#ffe0b2",
      dark: "#cbae82",
      contrastText: "#000000",
    },
  },
});

ReactDOM.render(
  <MuiThemeProvider theme={theme}>
    <React.StrictMode>
      <App />
    </React.StrictMode>
  </MuiThemeProvider>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
