// 使いやすいように最初にArrayで種々の名称を保持し、必要に応じてこれらを利用し型定義を行う
// 後の型定義のため、またプログラム内から意図せず内容を書き換えてしまうのを防ぐため as const する
// Read onlyになっているこれらのArrayを通常操作で利用する際に型推論例外が発生する場合は、
// [...layerSpecifierTags] のようにスプレッド構文を用いると
// string[] にキャスト可能になり例外回避できることが多いのを確認済み
export default class varsMTG {
  static readonly layerSpecifierTags = ["ベース名", "レアリティ", "レイヤー名"] as const;

  static readonly statusNames = ["物攻", "物防", "命中", "速さ", "魔攻", "魔防", "回復", "幸運"] as const;

  static readonly layerTypes = ["アタッカー", "ディフェンダー", "サポーター", "トリックスター"] as const;

  static readonly weaponTypes = ["剣", "弓", "斧", "杖", "槍", "拳", "銃", "鞭", "魔具"] as const;

  static readonly elements = ["刃", "衝", "貫", "火", "水", "風", "光", "闇"] as const;

  static readonly layerSpecifierVars = ["baseName", "rarity", "layerName"] as const;

  static readonly statusVars = ["phyAtk", "phyDef", "hit", "speed", "magAtk", "magdef", "heal", "luck"] as const;

  static readonly elemenVars = ["blade", "impact", "pierce", "fire", "water", "wind", "light", "darkness"] as const;
}
