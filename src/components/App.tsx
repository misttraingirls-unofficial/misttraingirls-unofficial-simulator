import React, { Component } from "react";
import Dashboard from "components/Dashboard";

export default class App extends Component {
  render(): JSX.Element {
    return <Dashboard />;
  }
}
