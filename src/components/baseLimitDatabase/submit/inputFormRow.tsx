import React from "react";

import TextField from "@material-ui/core/TextField";
import MenuItem from "@material-ui/core/MenuItem";
import Typography from "@material-ui/core/Typography";
import Input from "@material-ui/core/Input";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import IconButton from "@material-ui/core/IconButton";

import EditIcon from "@material-ui/icons/EditOutlined";
import DoneIcon from "@material-ui/icons/DoneAllTwoTone";
import { green, grey } from "@material-ui/core/colors";

import { menuProps, useBaseLimitPageStyles } from "components/baseLimitDatabase/style";
import { rowType } from "components/baseLimitDatabase/submit/limitFormRow";

import { varMapsMTG, layerDataType, typeTree } from "components/utils";
import varsMTG from "components/vars";

type layerDictType = Record<string, layerDataType["baseLimit"]>;
type rarityDicttype = Record<string, layerDictType>;
export type uncoveredBasesType = Record<string, rarityDicttype>;

const getRarityList = (uncoveredBases: uncoveredBasesType, baseName: string): string[] =>
  baseName === "" ? [] : Object.keys(uncoveredBases[baseName]);

const getLayerList = (uncoveredBases: uncoveredBasesType, baseName: string, rarity: string): string[] => {
  if (baseName === "" || rarity === "") {
    return [];
  }
  return rarity in uncoveredBases[baseName] ? Object.keys(uncoveredBases[baseName][rarity]) : [];
};

type CustomSelectFieldType = {
  value: string;
  name: string;
  row: rowType;
  onChange: (event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>, row: rowType, name: string) => void;
  options: string[];
};

const CustomSelectField: React.FC<CustomSelectFieldType> = (props) => {
  const { value, name, row, onChange, options } = props;
  const classes = useBaseLimitPageStyles();
  return (
    <TextField
      select
      value={value}
      onChange={(e) => onChange(e, row, name)}
      className={classes.inputSelect}
      margin="dense"
      fullWidth
      size="small"
      SelectProps={{
        MenuProps: menuProps(classes),
      }}
      InputProps={{
        classes: {
          input: classes.input,
        },
      }}
    >
      {options.map((option) => (
        <MenuItem key={option} value={option}>
          <Typography style={{ fontSize: 11 }}>{option}</Typography>
        </MenuItem>
      ))}
    </TextField>
  );
};

type InputFormCellType = {
  row: rowType;
  name: string;
  width: number;
  align: "left" | "center" | "right";
  onChange: (event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>, row: rowType, name: string) => void;
  uncoveredBases?: uncoveredBasesType;
  disabled?: boolean;
};

const InputFormCell: React.FC<InputFormCellType> = (props) => {
  const classes = useBaseLimitPageStyles();
  const { row, name, width, align, onChange, uncoveredBases = {}, disabled = false } = props;
  const { isEditMode } = row;
  const value = row[name];

  const options = (key: typeTree["layerSpecifierVar"]): string[] =>
    ({
      baseName: Object.keys(uncoveredBases).sort(),
      rarity: getRarityList(uncoveredBases, row.baseName).sort(),
      layerName: getLayerList(uncoveredBases, row.baseName, row.rarity).sort(),
    }[key]);

  let content: JSX.Element;

  if (!disabled && isEditMode) {
    if (varMapsMTG.layerSpecifier.has(name as typeTree["layerSpecifierVar"])) {
      content = (
        <CustomSelectField
          {...{
            value,
            name,
            row,
            onChange,
            options: options(name as typeTree["layerSpecifierVar"]),
          }}
        />
      );
    } else {
      content = (
        <Input
          value={value}
          name={name}
          onChange={(e) => onChange(e, row, name)}
          className={classes.input}
          margin="dense"
          autoComplete="off"
          fullWidth
        />
      );
    }
  } else {
    content = value;
  }

  return (
    <TableCell align={align} className={classes.tableCell} style={{ width }} size="small">
      {content}
    </TableCell>
  );
};

type InputFormRowType = {
  row: rowType;
  onToggleEditMode: () => void;
  onChange: (event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>, row: rowType, name: string) => void;
  uncoveredBases?: uncoveredBasesType;
  includeInvalidInput: boolean;
};

const InputFormRow: React.FC<InputFormRowType> = (props) => {
  const classes = useBaseLimitPageStyles();
  const { row, onToggleEditMode, onChange, uncoveredBases, includeInvalidInput } = props;
  const cellProps: { name: string; width: number; align: "left" | "center" | "right" }[] = [
    { name: "baseName", width: 160, align: "left" },
    { name: "rarity", width: 80, align: "left" },
    { name: "layerName", width: 200, align: "left" },
    { name: "hp", width: 70, align: "center" },
  ];
  return (
    <TableRow>
      <TableCell className={classes.selectTableCell} size="small">
        {row.isEditMode ? (
          <IconButton
            aria-label="done"
            size="small"
            onClick={onToggleEditMode}
            style={{ color: includeInvalidInput ? grey[500] : green[500], fontSize: 14 }}
          >
            <DoneIcon style={{ fontSize: 14 }} />
          </IconButton>
        ) : (
          <IconButton aria-label="delete" size="small" onClick={onToggleEditMode}>
            <EditIcon style={{ fontSize: 14 }} />
          </IconButton>
        )}
      </TableCell>
      {cellProps.map((cellProp) => (
        <InputFormCell
          key={`form.${cellProp.name}`}
          {...{
            row,
            onChange,
            uncoveredBases,
            ...cellProp,
          }}
        />
      ))}
      {varsMTG.statusVars.map((key) => (
        <InputFormCell
          key={`form.${key}`}
          {...{
            row,
            name: key,
            width: 70,
            align: "center",
            onChange,
          }}
        />
      ))}
    </TableRow>
  );
};
export default InputFormRow;
