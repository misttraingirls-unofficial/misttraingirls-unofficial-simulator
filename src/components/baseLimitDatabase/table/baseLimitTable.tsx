import React, { PureComponent } from "react";
import withStyles, { WithStyles } from "@material-ui/core/styles/withStyles";
import Table from "@material-ui/core/Table";
import TableContainer from "@material-ui/core/TableContainer";
import TableBody from "@material-ui/core/TableBody";
import TableHead from "@material-ui/core/TableHead";

import Title from "components/Title";
import { baseLimitPageStyles } from "components/baseLimitDatabase/style";

import CustomTableRow, { rowType } from "components/baseLimitDatabase/table/customTableRow";
import CustomTableHeader, { baseNameFilterOptionsType } from "components/baseLimitDatabase/table/customTableHeader";
import { CaptionHeader, CaptionFooter } from "components/baseLimitDatabase/table/caption";

interface BaseLimitTableProps extends WithStyles<typeof baseLimitPageStyles> {
  loadDatabase: () => void;
  baseNames: { name: string; key: string }[];
  filterAll: string;
  originalRows: rowType[];
  rows: rowType[];
  previous: { [id: string]: rowType };
  sortOrder: "asc" | "desc";
  sortKey: string;
  filterBase: { name: string; key: string } | null;
  onInputChange: (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>, id: string) => void;
  onToggleEditMode: (id: string) => void;
  onRevert: (id: string) => void;
  onChangefilter: (event: React.ChangeEvent<unknown>, value: { name: string; key: string } | null) => void;
  baseNameFilterOptions: baseNameFilterOptionsType;
  handleClickSortColumn: (key: string) => () => void;
}

class BaseLimitTable extends PureComponent<BaseLimitTableProps> {
  // constructor(props) {
  //   super(props);
  // }

  render() {
    const {
      classes,
      baseNames,
      sortKey,
      sortOrder,
      rows,
      onInputChange,
      onToggleEditMode,
      onRevert,
      onChangefilter,
      baseNameFilterOptions,
      handleClickSortColumn,
    } = this.props;

    return (
      <>
        <Title>基準値からの増減表</Title>
        <CaptionHeader />
        <TableContainer className={classes.tableContainer}>
          <Table stickyHeader className={classes.table} aria-label="caption table" size="small">
            <TableHead>
              <CustomTableHeader
                {...{
                  sortKey,
                  sortOrder,
                  handleClickSortColumn,
                  baseNames,
                  onChangefilter,
                  baseNameFilterOptions,
                }}
              />
            </TableHead>
            <TableBody>
              {rows.map((row) => (
                <CustomTableRow key={row.id} {...{ row, onToggleEditMode, onRevert, onInputChange }} />
              ))}
            </TableBody>
          </Table>
        </TableContainer>
        <CaptionFooter />
      </>
    );
  }
}
export default withStyles(baseLimitPageStyles, { withTheme: true })(BaseLimitTable);
