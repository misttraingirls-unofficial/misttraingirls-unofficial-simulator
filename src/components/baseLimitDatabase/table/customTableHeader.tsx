import React from "react";
import { withStyles, createStyles } from "@material-ui/core/styles";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import TextField from "@material-ui/core/TextField";
import TableSortLabel from "@material-ui/core/TableSortLabel";
import { FilterOptionsState } from "@material-ui/lab";
import Autocomplete, { AutocompleteRenderInputParams } from "@material-ui/lab/Autocomplete";
import Typography from "@material-ui/core/Typography";

import { menuProps, useBaseLimitPageStyles } from "components/baseLimitDatabase/style";
import { varMapsMTG } from "components/utils";

const StyledTableSortLabel = withStyles(() =>
  createStyles({
    icon: {
      fontSize: 10,
    },
  })
)(TableSortLabel);

export type baseNameFilterOptionsType = (
  options: { name: string; key: string }[],
  { inputValue }: FilterOptionsState<{ name: string; key: string }>
) => { key: string; name: string }[];

type BaseNameFilterAutocompleteType = {
  baseNames: { name: string; key: string }[];
  onChangefilter: (event: React.ChangeEvent<unknown>, value: { name: string; key: string } | null) => void;
  baseNameFilterOptions: baseNameFilterOptionsType;
};

const BaseNameFilterAutocomplete: React.FC<BaseNameFilterAutocompleteType> = (props) => {
  const classes = useBaseLimitPageStyles();
  const { baseNames, onChangefilter, baseNameFilterOptions } = props;
  return (
    <Autocomplete
      options={baseNames as { name: string; key: string }[]}
      getOptionLabel={(option) => option.name}
      // getOptionSelected={(option: {key: string, name: string}, value: {key: string, name: string}) => option.key === value || option.key === value.key}
      getOptionSelected={(option, value) => option.key === value.key}
      onChange={onChangefilter}
      id="basename-select"
      size="small"
      renderOption={(option) => <Typography style={{ fontSize: 11 }}>{option.name}</Typography>}
      filterOptions={baseNameFilterOptions}
      renderInput={(params: AutocompleteRenderInputParams) => (
        <div ref={params.InputProps.ref}>
          <TextField
            margin="dense"
            size="small"
            style={{ width: 160, height: 14 }}
            className={classes.baseNameSelect}
            {...params}
            InputProps={{
              ...params.InputProps,
              classes: {
                input: classes.inputFilter,
              },
            }}
            SelectProps={{
              // ...params.SelectProps,
              MenuProps: menuProps(classes),
            }}
          />
        </div>
      )}
    />
  );
};

type CustomTableHeaderType = {
  sortOrder: "asc" | "desc";
  sortKey: string;
  baseNames: { name: string; key: string }[];
  onChangefilter: (event: React.ChangeEvent<unknown>, value: { name: string; key: string } | null) => void;
  baseNameFilterOptions: baseNameFilterOptionsType;
  handleClickSortColumn: (key: string) => () => void;
};

const CustomTableHeader: React.FC<CustomTableHeaderType> = (props) => {
  const classes = useBaseLimitPageStyles();
  const { sortKey, sortOrder, handleClickSortColumn, baseNames, onChangefilter, baseNameFilterOptions } = props;
  // 本来の矢印の向きだと違和感があるため反転
  const sortDirection = sortOrder === "desc" ? "asc" : "desc";

  return (
    <TableRow>
      <TableCell
        align="center"
        size="small"
        className={classes.basenameSorter}
        sortDirection={sortKey === "baseName" ? sortOrder : false}
      >
        <StyledTableSortLabel
          active={sortKey === "baseName"}
          direction={sortDirection}
          onClick={handleClickSortColumn("baseName")}
        />
      </TableCell>
      <TableCell align="left" colSpan={2}>
        <BaseNameFilterAutocomplete {...{ baseNames, onChangefilter, baseNameFilterOptions }} />
      </TableCell>
      <TableCell align="center" size="small" className={classes.tableHeader} />
      {[...varMapsMTG.status].map(([key, name]) => (
        <TableCell
          key={`${key}.header`}
          align="right"
          size="small"
          className={classes.tableHeader}
          sortDirection={sortKey === key ? sortOrder : false}
        >
          <StyledTableSortLabel active={sortKey === key} direction={sortDirection} onClick={handleClickSortColumn(key)}>
            {name}
          </StyledTableSortLabel>
        </TableCell>
      ))}
    </TableRow>
  );
};
export default CustomTableHeader;
