/* eslint-disable react/jsx-wrap-multilines */
import React from "react";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import ListSubheader from "@material-ui/core/ListSubheader";
import DashboardIcon from "@material-ui/icons/Dashboard";
import AssignmentIcon from "@material-ui/icons/Assignment";
import Typography from "@material-ui/core/Typography";

const disabledColor = "#888888";

export const databaseMenuItems = (
  <div>
    <ListSubheader inset>Database</ListSubheader>
    <ListItem button>
      <ListItemIcon>
        <DashboardIcon fontSize="small" />
      </ListItemIcon>
      <ListItemText
        disableTypography
        primary={
          <Typography variant="body2" style={{ color: disabledColor }} gutterBottom>
            レイヤー基本情報
          </Typography>
        }
      />
    </ListItem>
    <ListItem button>
      <ListItemIcon>
        <DashboardIcon fontSize="small" />
      </ListItemIcon>
      <ListItemText
        disableTypography
        primary={
          <Typography variant="body2" style={{ color: "#000000" }} gutterBottom>
            ベース能力値成長限界
          </Typography>
        }
      />
    </ListItem>
  </div>
);

export const simulatorMenuItems = (
  <div>
    <ListSubheader inset>Simulator</ListSubheader>
    <ListItem button>
      <ListItemIcon>
        <AssignmentIcon fontSize="small" />
      </ListItemIcon>
      <ListItemText
        disableTypography
        primary={
          <Typography variant="body2" style={{ color: disabledColor }} gutterBottom>
            戦闘計算式検証ツール
          </Typography>
        }
      />
    </ListItem>
    <ListItem button>
      <ListItemIcon>
        <AssignmentIcon fontSize="small" />
      </ListItemIcon>
      <ListItemText
        disableTypography
        primary={
          <Typography variant="body2" style={{ color: disabledColor }} gutterBottom>
            戦闘シミュレータ
          </Typography>
        }
      />
    </ListItem>
  </div>
);
