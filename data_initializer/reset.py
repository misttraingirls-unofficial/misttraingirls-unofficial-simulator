import os
import json
import glob
import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore

# 以下はローカルに保存される非公開の秘密鍵へのパス
os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = os.path.expanduser(
    '~/.firebase/misttraingirls-unofficial/adminsdk.json')
DATA_DIR_NAME = "./initial_database"


def main():
    # Use the application default credentials
    cred = credentials.ApplicationDefault()
    firebase_admin.initialize_app(
        cred, {"projectId": "misttraingirls-unofficial"})

    db = firestore.client().collection(u'database')

    for path in glob.iglob(os.path.join(DATA_DIR_NAME, "*")):
        doc_name = os.path.splitext(os.path.basename(path))[0]
        doc_ref = db.document(doc_name)
        print(f"setting up a document: [{doc_name}]")

        if os.path.isdir(path):
            tmp_dct = {}
            for filename in glob.iglob(os.path.join(path, "**/*.json"), recursive=True):
                print(f"  reading {os.path.basename(filename)}")
                with open(filename, "r") as f:
                    tmp_dct.update(json.load(f))
            doc_ref.update(tmp_dct)
        else:
            print(f"  reading {os.path.basename(path)}")
            with open(path, "r") as f:
                doc_ref.set(json.load(f))


if __name__ == "__main__":
    main()
