module.exports = {
  env: {
    browser: true,
    es6: true,
    node: true,
  },
  extends: [
    "plugin:react/recommended",
    "airbnb",
    "plugin:jest/recommended",
    "plugin:jest/style",
    "react-app",
    "react-app/jest",
    "prettier",
    "plugin:@typescript-eslint/eslint-recommended",
    "plugin:@typescript-eslint/recommended",
    "prettier/@typescript-eslint",
  ],
  globals: {
    Atomics: "readonly",
    SharedArrayBuffer: "readonly",
  },
  parser: "@typescript-eslint/parser",
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 2020,
    sourceType: "module",
  },
  plugins: ["babel", "jest", "@typescript-eslint"],
  rules: {
    "no-console": "off",
    "react/prefer-stateless-function": 0,
    "react/jsx-filename-extension": [1, { extensions: [".js", ".jsx", ".ts", ".tsx"] }],
    "react/prop-types": "off",
    // "import/no-extraneous-dependencies": ["error", {"devDependencies": true}],
    "react/default-props-match-prop-types": "off",
    "react/jsx-props-no-spreading": "off",
    "react/require-default-props": "off",
    "import/extensions": [
      "error",
      "ignorePackages",
      {
        js: "never",
        jsx: "never",
        ts: "never",
        tsx: "never",
      },
    ],
  },
  // reference: https://stackoverflow.com/questions/60547153/eslint-parsing-error-unexpected-token-on-typescript-cast-operator-as
  settings: {
    react: {
      pragma: "React",
      version: "detect",
      flowVersion: "0.53",
    },
    overrides: [
      {
        files: ["*.js", "*.jsx"],
        rules: {
          "@typescript-eslint/explicit-function-return-type": "off",
        },
      },
    ],
    "import/resolver": {
      node: {
        extensions: [".js", ".jsx", ".ts", ".tsx"],
        moduleDirectory: ["node_modules", "src/"],
      },
    },
  },
};
